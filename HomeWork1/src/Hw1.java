public class Hw1 {

    public  int fourth(int a, int b)
    {
        int res;
        if (a > 0 && b > 0)
        {
            res = 1;
        }
        else if (a < 0 && b > 0)
        {
            res = 2;
        }
        else if (a < 0 && b < 0)
        {
            res = 3;
        }
        else res = 4;
        return res;
    }

    public  int sumOrAdd(int a, int b)
    {
        int res;
        if (a % 2 == 0)
        {
            res = a * b;
        } else
        {
            res = a + b;
        }
        return res;
    }



    public  int sumPositive(int a, int b, int c)
    {
        int res = 0;
        if (a > 0)
        {
            res += a;
        }
        if (b > 0)
        {
            res += b;
        }
        if (c > 0)
        {
            res += c;
        }
        return res;
    }


    public int sumMax(int a, int b, int c)
    {
        int res;
        int x = a * b * c;
        int y = a + b + c;
        if (x > y)
        {
            res = x;
        }
        else res = y;
        return res + 3;
    }


    public  char studentGrade(int a)
    {
        char res = 0;
        if (a >= 0 && a <= 19)
        {
            res = 'F';
        }
        else if (a >= 20 && a <= 39)
        {
            res = 'E';
        }
        else if (a >= 40 && a <= 59)
        {
            res = 'D';
        }
        else if (a >= 60 && a <= 74)
        {
            res = 'C';
        }
        else if (a >= 75 && a <= 89)
        {
            res = 'B';
        }
        else if (a >= 90 && a <= 100)
        {
            res = 'A';
        }
        return res;
    }
}
